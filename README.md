# Concept

1) Factorio meets Universal Paperclip

One of the frustrating things with Factorio is the physical logistics of moving stuff around on a surface. It's both interesting, and tedious.

OTOH, Universal Paperclip has a very, very, very bleak outcome.

2) Time limit - you only have so long to complete the mission before the human settlers arrive, and if the planet isn't sufficiently Earth-like, they all die.

3) Research - your automated ship crashed and virtually all of the schematic data was lost.  You can create computers to rebuild the schematics from the remaining fragments and checksums, but it is a very long process

4) You can't pause the game while you're playing it - the slowest you can go is real-time.  But you have 30 years to complete the project, so that's not so bad

5) Drone logistics - drones can only carry so much weight, which limits your scope, at least at the beginning

6) Miners dig up whatever they dig up. Some plots may be richer in certain minerals than they are in others, but you're going to have a lot of silica mixed in with your copper.  Each dig plot has its own particular mix, with some rare metals and such being "bonuses"

7) Multiple planets (goal) - ideally, converting an Earth-equivalent-but-with-no-atmosphere planet into another Earth is just the first step - you should be able to Earthify a Venusian planet by chilling it, ejecting frozen carbon dioxide and such, and then carefully recovering it.  You should be able to Earthify a Martian planet by adding atmosphere and adding weight to the planet core via metals mined from space. And to Earthify a Mercurial planet by moving it and increasing the spin.  Titan (or whichever one has all the methane) by ejecting it from gas giant orbit, moving it, and dealing with all the gasses.

8) Start simple - create a planet with a small set of required minerals, just so you can explore the concepts

9) Saveable - make sure I understand how React deals with cookies, so I can save the game properly. 








# Old Stuff Below

Just this game, y'know?


thinking about this further as I explore the concept of Factorio meets
Universal Paperclip.




1) It's a PITA, but I definitely want to include day and night, since it makes
batteries more important.

2) You start off with miners and refiners and assemblers.   We should probably
add some additional elements - sodium, sulfur, platinum, maybe lithium? and
the generic "rare metals" .  Plutonium?

3) Your machines emit "toxic gasses" as a byproduct of operations, and you
can't have more than 1 PPM of that in the breathable atmosphere.

4) So you're already balancing emissions vs efficiency because you only have
so long before the humans arrive.

This is technically all you would need to complete the terraforming project.
But you can also pursue various advanced facilities:

A) Large Assembler - a large device made up of <N> transports and <M>
assemblers, that can assemble larger projects, including

   A1) Toxin Scrubber  - cleans toxins out of the atmosphere, uses
   electricity, converts toxins to rare metals.

   A2) Autonomous Assembler Factory - Build this and it will begin producing
   units to produce whatever seems to be the most in demand

   A3) Meteor Defense Towers

   A4) Geothermic Radiator

   A5)

   A6) Soil Enrichment

   A7) Mega Assembler...

B) Mega Assembler - extremely large device, capable of producing A1..AN in
much less time, as well as:

   B1)  Orbital Transport Rocket

   B2)  Remote Planet Analysis Telescope

   B3)  Space Elevator

   B4)  Autonomous Large Assembler Factory  (tricky...)

   B5)  Matter Converter

   B6)  Bioassembler

   B7)  Orbital Assembler...

C) Orbital Assembler -

   C1) Meteor Defense Sattelite

   C2) Comet Harvester

   C3) Interstellar Vehicle

   C4) Interstellar Power Core

   C5) Interstellar Mach Engine

   C6) Child Autonomous Terraforming Engine (CATE)

   C7) Cosmic Ray Deflector






2018-03-30

A new idea on presentation - a spreadsheet-like display, with reports on elements and status at the top, and then
a scroll down with layers upon layers of intermediate production items, which light up when you have the necessary raw
materials to build them.

We would need some fun display tricks, for example:

a) click on the button that builds an item, and we display a progress bar on the button
b) hover over a button, and it highlights, and shows lines to all the previous products that are required to build it
b.1) Ideally, with different colors for each of the predecessor items.


using this model simplifies things in some ways, because I don't have to try to fit everything on one page, and I don't
have to keep changing the story like paperclips does.  


Game stressors:
1) You can run out of battery and/or electrical generation, and that kills your production, obviously.  
q1) Do we want to degrade performance of everything, or do we want to shut down, or a mix?   Presentation is a challenge here.

2) You can run out of time, which is really the game-ender

3) Runaway processes?  (Too much CO2 causes runaway greenhouse, and now you have to change your mechanical strategy?)

4) Events - earthquakes, fires, solar flares, eclipses, meteor strikes - damage equipment, etc

5) Success at one level causes problems at the next - adding CO2, for example.  Or too much oxygen in the atmosphere creates
increased risk of fire.

Stress Relievers:

1) Research - more efficient mining, more productive mining, more efficient refining, more productive refining, better transport logistics, etc

2) New stuff that automates production of lower-level stuff




Iterative approach:

1) Can I make a presentation that looks like a spreadsheet?   Ideally with multiple tabs - whatever you click on in the first tab has a help
screen as the second tab

2) Can I create the tick that drives the spreadsheet numbers over time

3) Can I change the units of production as necessary for readability?

4) Can I light up cells when the ingredients they require are available?

5) Can I create a progress bar delay while items are being built.

6) Later items are automated, perhaps they have progress bars showing their production in an automated way?




Maybe when you click on a cell, the formula bar shows what you can do with that entity - create 1, 10, 100, destroy 1, 10, 100, current build progress?
That's nice, because putting all of that in a single cell or pair of cells is a PITA.


time is in the upper right, and the rate of time passage can be changed

Below the time are the goals, and current status towards those objectives.


Atmospheric pressure
Average surface temperature
atmospheric nitrogen, oxygen, carbon dioxide, water vapor
surface water
surface ice  (for every degree above freezing, one percent of the ice is converted into water at some rate.  
For every degree above X, one percent of the surface water is turned into water vapor at some rate.
gravity
magnetosphere strength



2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 2021-09-13

Some ideas

1) There's an array of world files.  Each world file describes all the constraints of that particular world:
  - the victory conditions
  - the starting resources
  - the available research
  - the time constraints
  - the world parameters


  this makes it easy to support multiple worlds (and in fact, I can easily establish a very simple first world for training / testing purposes)

2) the models and research are fixed - i.e. the individual worlds draw on them by reference, instead of describing them locally.  That gives me
  the flexibility to describe them in code if I need do

3) 






--------------------------------------------------------------


Going to use React to help with managing the UI.  I like the logic ---> widget model, it makes the development easier


we'
re close, but I'm too tired to do this development tonight.  Sorry

The imporant things are:


a. It's fun

b.   its challenging

c.  it's not frustrating to play

d.  It inspires people to be better

e.  it teaches me more about react programming, and redux and other key libraries

f.  it elegantly supports new research, new concept sand new machines in a smart way


