
import {
    Link,
  } from "react-router-dom";
  
import { useCookies } from 'react-cookie';
import { useHistory } from "react-router-dom";


export default function HardReset() {

    const history = useHistory();

    // removed cookies, setCookie to supress warning
    const [, , removeCookie] = useCookies(['earthify_name', 'earthify_save']);


    function clearCookies() {
        removeCookie('earthify_name', { path: '/' });
        removeCookie('earthify_save', { path: '/' });
        history.push("/");
    }

    return(
        <div>
            <h1>Start Over?</h1>
            <Link to="/">Oops! Go Back!</Link>
            <button onClick={clearCookies}>Do it!</button>
        </div>
    )

}