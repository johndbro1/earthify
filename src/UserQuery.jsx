import React, { useState } from "react";
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import FilledInput from '@material-ui/core/FilledInput';
import Button from '@material-ui/core/Button';
import Grid from "@material-ui/core/Grid";
import { makeStyles } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';

import SelfTypingConsole from './SelfTypingConsole';

const useStyles = makeStyles((theme) => ({
    root: {
      background: "white"
    },
    input: {
      color: "black"
    }
}));


function UserQuery(props) {

    const classes = useStyles();

    const [displayStage, setDisplayStage] = useState(0);
    const [answer, setAnswer] = useState('');


      

    const textSequence = [
        props.prompt 
    ];


    const handleSubmit = (evt) => {
        evt.preventDefault();
        props.onChange(answer);
    }


    return(
        <div>
        {displayStage === 0 &&  <SelfTypingConsole lines={4} endDelay={1000} startDelay={1000} speed={35} text={textSequence} onComplete={()=>setDisplayStage( (s) => s+1 )}  /> }
        {displayStage === 1 && 
            <>
                {props.prompt} 
                <form noValidate autoComplete="off" onSubmit={handleSubmit}>
                <Grid container alignItems="center" alignContent="center" justifyContent="center" direction="column">
                    <Grid item>
                        <TextField variant="filled"
                            value={answer}
                            className={classes.root}                         
                            InputProps={{ 
                                className: classes.input
                            }}
                            onChange={e => setAnswer(e.target.value)}
                        />
                        <Button variant="contained" color="primary" type="submit">Submit</Button>
                    </Grid>
                </Grid>                  
                </form>            
            </>
        }
        </div>
    );

}

export default UserQuery;