import React, { useState } from "react";

export default function NameForm(props) {
    const [name, setName] = useState("");
  
    const handleSubmit = (evt) => {
        evt.preventDefault();
        props.onChange(name);
    }
    
    return (
      <form onSubmit={handleSubmit}>
        <label>
          What is your designation?:
          <input
            type="text"
            value={name}
            onChange={e => setName(e.target.value)}
          />
        </label>
        <input type="submit" value="Submit" />
      </form>
    );
  }