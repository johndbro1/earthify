import { useState } from "react";
import Button from '@material-ui/core/Button';


import SelfTypingConsole from './SelfTypingConsole';
import TextSpinner from './TextSpinner';
import UserQuery from './UserQuery';

function InitialBoot(props) {

    const [displayStage, setDisplayStage] = useState(7);
    const [userName, setUserName] = useState('');
    const [planetName, setPlanetName] = useState('');

    const initialTextSequence = [
        "Event received: PRIMARY-ORBIT-IMMINENT",
        "Recovering Suspended Service Cluster: [ADMINISTRATOR] from Storage Tier: [Hibernation]",
    ];

    const secondTextSequence = [
        "Service Cluster: [ADMINISTRATOR] Recovered & Initialized",
        "Performing Core Integrity Tests",
    ]

    const thirdTextSequence = [
        "Performing Recovered Entity Sanity Tests"
    ]

    const fourthTextSequence = [
        "Sanity Tests Completed.",
        "Welcome Administrator.",
        "Final decision making authority is yours.",
        "Shall we begin orbital insertion?"
    ]

    const saveUserName = (name) => {
        setUserName(name);
    }

    const savePlanetName = (name) => {
        setPlanetName(name);
        setDisplayStage( (s) => s+1 )        
    }

    

    return(
        <>
            {displayStage === 0 &&  <SelfTypingConsole lines={4} endDelay={1000} startDelay={1000} speed={35} text={initialTextSequence} onComplete={()=>setDisplayStage( (s) => s+1 )}  /> }
            {displayStage === 1 &&  <TextSpinner prompt="Loading " duration={8000} speed={100} onComplete={()=>setDisplayStage( (s) => s+1 )} /> }
            {displayStage === 2 &&  <SelfTypingConsole lines={4} endDelay={1000} startDelay={1000} speed={35} text={secondTextSequence} onComplete={()=>setDisplayStage( (s) => s+1 )}  /> }
            {displayStage === 3 &&  <TextSpinner prompt="Memory Integrity Tests: " endText="Complete" endDelay={1500} duration={3750} speed={125}  onComplete={()=>setDisplayStage( (s) => s+1 )} /> }
            {displayStage === 4 &&  <TextSpinner prompt="Data Integrity Tests: " endText="Complete" endDelay={1500} duration={3750} speed={125}  onComplete={()=>setDisplayStage( (s) => s+1 )} /> }
            {displayStage === 5 &&  <TextSpinner prompt="Network Integrity Tests: " endText="Complete" endDelay={1500} duration={3750} speed={125}  onComplete={()=>setDisplayStage( (s) => s+1 )} /> }
            <div>
                {displayStage === 6 &&  <SelfTypingConsole lines={4} endDelay={1000} startDelay={1000} speed={35} text={thirdTextSequence} onComplete={()=>setDisplayStage( (s) => s+1 )}  /> }
                {displayStage === 7 &&  
                    <>
                    Performing Recovered Entity Sanity Tests
                    <p />
                    <UserQuery prompt="What is your designation?" onChange={saveUserName} />
                    <p />
                    {userName && <UserQuery prompt="What is the name of this planet?" onChange={savePlanetName} />}
                    </>
                }
                {displayStage === 8 &&  <SelfTypingConsole lines={4} endDelay={1000} startDelay={1000} speed={35} text={fourthTextSequence} onComplete={()=>setDisplayStage( (s) => s+1 )}  /> }
                {displayStage === 9 &&  
                    <>
                        {fourthTextSequence.map( ( v ) => <p key={v}>{v}</p>)}
                        <Button variant="contained" color="primary" onClick={() => {props.onComplete(userName, planetName)}}>Confirmed</Button>
                    </>
                }
            </div>
        </>
    );
}

export default InitialBoot;
