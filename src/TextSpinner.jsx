import { useState, useEffect } from "react";



export default function TextSpinner( props ) {

    const [ data, setData ] = useState(  {   index: -1, element: '', elapsed: 0, endText: null});


    const spinDuration = props.duration || 5000;
    const endDuration  = props.endDelay || 500;
    const speed    = props.speed || 50;
    const charList = props.charList || "|/-\\|/-\\";
    const endText  = props.endText || '';


    useEffect( () => {

        function cycleToNextCharacter( str ) {

            let tmp = { ...data };
            
            tmp.index++;

            if (tmp.index >= charList.length) {
                tmp.index = 0;
            }

            tmp.element = charList.charAt(tmp.index);

            tmp.elapsed += speed;

            setData( tmp );
        }


        function displayEndText() {

            let tmp = { ...data };

            tmp.endText = endText;

            setData(tmp);
        }


        if (data.elapsed >= spinDuration) {
            
            if (data.endText === null) {

                displayEndText()

            } else {

                setTimeout( () => {
                    if (props.onComplete) {
                        props.onComplete();                
                    }
                }, endDuration);
    
            }

            return;

        }

        setTimeout(  () => {
            cycleToNextCharacter(charList);
        }, speed);


    }, [props, data, charList, spinDuration, endDuration, endText, speed])


    return(
        <>
            {props.prompt} {data.element} {data.endText}
        </>
    )
}