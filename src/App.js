import './App.css';

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";


import Earthify from './Earthify';
import HardReset from './HardReset';

export default function App() {

  return(
    <Router>
      <Switch>
        <Route path="/reset">
          <HardReset />
        </Route>
        <Route path="/">
          <Earthify />
        </Route>
      </Switch>
    </Router>
  )
}


