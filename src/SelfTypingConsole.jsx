import { useState, useEffect } from "react";

export default function SelfTypingConsole( props ) {
    
    const [ indexObj, setIndexObj ] = useState( { propsTextLineIndex: -1, charIndex: -1, outputLineIndex: -1, output: []})

    const endDelay = props.endDelay || 1500;


    useEffect( () => {
        


        function prepareForNewLine() {

            let tmp = { 
                    propsTextLineIndex: indexObj.propsTextLineIndex +1, 
                    charIndex: 0, 
                    outputLineIndex: indexObj.outputLineIndex, 
                    output: [...indexObj.output] 
                };

            // props.lines defines how many lines of text show up in our console.  If we 
            // go past the specified size, we need to discard the first line of the output window
            // and set element 0 to have the value of element 1, element 1 to have the value of 
            // element 2, etc.
            if (indexObj.output.length < props.lines) {

                // console.log(`prepareForNewLine: appending line to existing output window`);
                
                tmp.output = [...indexObj.output, ''];
                tmp.outputLineIndex = tmp.outputLineIndex+1;
            } else {

                if (tmp.propsTextLineIndex < props.text.length) {
                    // console.log(`prepareForNewLine: slicing out first element`);

                    tmp.output = tmp.output.slice(1);    
                } else {
                    // console.log(`not slicing out first element because we've completed rendering`);
                }

            }
    
            setIndexObj( tmp );
    
            // console.log(`prepareForNewLine: ${JSON.stringify(tmp)}`);
        }
    
    
        function completeLine()  {
    
            let tmp = { 
                propsTextLineIndex: indexObj.propsTextLineIndex, 
                charIndex: -1, 
                output: indexObj.output, 
                outputLineIndex: indexObj.outputLineIndex
            };
    
            setIndexObj(tmp);
    
            // console.log(`completeLine: ${JSON.stringify(tmp)}`);
    
        }
    
        function incrementCurrentLine( newOutput ) {
    
            // console.log(`incrementCurrentLine: newOutput is: ${newOutput}`);

            let tmp = { 
                propsTextLineIndex: indexObj.propsTextLineIndex, 
                charIndex: indexObj.charIndex+1, 
                output: newOutput, 
                outputLineIndex: indexObj.outputLineIndex
            };
    
            setIndexObj(tmp);
    
            // console.log(`incrementCurrentLine: ${JSON.stringify(tmp)}`);
    
        }
    


        // console.log(`useEffect: indexObj: ${JSON.stringify(indexObj)}`);

        const lineIndex       = indexObj.propsTextLineIndex;
        const charIndex       = indexObj.charIndex;
        const output          = indexObj.output;
        const outputLineIndex = indexObj.outputLineIndex;

        // return if we've written everything we neede to write
        if (lineIndex >= props.text.length) {

            // console.log(`all text rendered, calling onComplete if available`);
            if (props.onComplete) {
                props.onComplete();
            }
            return;
        }

        // delay the start of typing
        if (charIndex === -1) {

            setTimeout( () => {
                prepareForNewLine();
            }, props.startDelay);
            return;
        }

        // console.log(`working line: lineIndex: ${lineIndex} . charIndex: ${charIndex} . full line: ${props.text[lineIndex]}  indexObj: ${JSON.stringify(indexObj)}`);

        // when we hit the end of the line, increment to the next line
        if (charIndex > props.text[lineIndex].length) {

            // console.log(`reached end of line: ${lineIndex}`);
            setTimeout( () => {
                completeLine();
            }, endDelay);
            return;
        }

        // add the individual characters
        const timeout = setTimeout( () => {

            let tmpArray = [...output];

            // console.log(`incremental 1: tmpArray is: ${tmpArray}`);

            tmpArray[outputLineIndex] = props.text[lineIndex].substr(0, charIndex );

            // console.log(`incremental 2: tmpArray is: ${tmpArray}`);

            incrementCurrentLine( tmpArray );
            
        }, props.speed);

        return () => clearTimeout( timeout );

    }, [props, indexObj, endDelay]);


    return (
        <div>
            {indexObj.output.map( (line, index) => <p key={index}>{line}</p>)}
        </div>
    )

}