import Grid from '@material-ui/core/Grid';

import { useCookies } from 'react-cookie';
import InitialBoot from './InitialBoot';
import MainGameLoop from './MainGameLoop';
import CrashShip from './CrashShip';

function Earthify() {




  const [cookies, setCookie] = useCookies(['earthify_name', 'earthify_save']);

  function designationChange(newName, newPlanet) {
    setCookie('earthify_name', newName, { path: '/' });
    setCookie('earthify_planet', newPlanet, { path: '/' });
  }

  function gameSetupComplete(saveData) {
    setCookie('earthify_save', saveData, { path: '/' });
  }

  return (
    <div className="App">
      <header className="App-header">
        <h2>Earthify</h2>
      </header>
      <div className="Main">
          {!cookies.earthify_name && <InitialBoot onComplete={designationChange} />}
          {cookies.earthify_name && !cookies.earthify_save && <CrashShip onChange={gameSetupComplete} />}
          {cookies.earthify_name && cookies.earthify_save && <MainGameLoop />}
      </div>
    </div>
  );
}

export default Earthify;
